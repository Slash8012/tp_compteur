﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CompteurLibrary;

namespace CompteurTest
{
    [TestClass]
    public class CompteurTest
    {
        [TestMethod]
        public void IncrementTest()
        {
            CompteurClass.IncrementationCompteur();
            Assert.AreEqual(1,CompteurClass.Counter);
        }

        public void DecrementationCompteur()
        {
            CompteurClass.DecrementationCompteur();
            Assert.AreEqual(0, CompteurClass.Counter);
            CompteurClass.DecrementationCompteur();
            Assert.AreEqual(0, CompteurClass.Counter);
        }

        public void RemiseAzero()
        {
            CompteurClass.RemiseAzero();
            Assert.AreEqual(0, CompteurClass.Counter);
        }
    }
}
