﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompteurLibrary
{
    public class CompteurClass
    {
        
        public static int Counter=0;

       
        public static void IncrementationCompteur()
        {
            Counter = Counter + 1;
        }

        public static void DecrementationCompteur()
        {
            Counter = Counter - 1;
            if (Counter < 0)
                Counter = 0;
        }

        public static void RemiseAzero()
        {
            Counter = 0;
        }
        
    }
}
