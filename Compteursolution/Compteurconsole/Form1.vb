﻿Imports CompteurLibrary

Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        CompteurClass.DecrementationCompteur()
        Label2.Text = CompteurClass.Counter


    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        CompteurClass.IncrementationCompteur()
        Label2.Text = CompteurClass.Counter


    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        CompteurClass.RemiseAzero()
        Label2.Text = CompteurClass.Counter
    End Sub


End Class
